if [ $(uname) = "Darwin" ]; then
    export IS_DARWIN="true"
    export IS_CYGWIN=""
    export PYTHON="python2.7"
elif [ $(uname -o) = "Cygwin" ]; then
    export IS_DARWIN=""
    export IS_CYGWIN="true"
    export PYTHON=$(cygpath -ua "C:\\Python27\\python.exe")
    export CYGWIN="${CYGWIN} nodosfilewarning"
else
    export IS_DARWIN=""
    export IS_CYGWIN=""
    export PYTHON="python2.7"
fi

function realpath {
    "${PYTHON}" -c "import os, sys; print os.path.abspath(sys.argv[1])" "$1"
}

THIS_PATH=$(realpath $(dirname ${BASH_SOURCE[0]}))
PROJECT_ROOT=$(realpath "$THIS_PATH/..")
ENV_PATH="${PROJECT_ROOT}/.env"

# Add the directory wiyh FF WebDriver executable to the system path
PATH="${PROJECT_ROOT}:${PATH}"

# Point to the FF binary executable on Mac OS
if [ "${IS_DARWIN}" ]; then
    export FF_BINARY="/Applications/Firefox.app/Contents/MacOS/firefox-bin"
fi