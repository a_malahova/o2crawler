#!/bin/bash

source $(dirname $0)/env.sh

# Activate virtualenv
source "${ENV_PATH}/bin/activate"

# Run crawler script
python "${PROJECT_ROOT}/py"
