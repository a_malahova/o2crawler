#!/bin/bash
source $(dirname $0)/env.sh
set -x

# install system package dependensies
if [ IS_CYGWIN = "true" ]; then
    sudo apt-get install python-pip virtualenv
elif [ IS_DARWIN = "true" ]; then
    brew install python-pip virtualenv
fi

# create virtualenv
if [ ! -d "$ENV_PATH" ]; then
    virtualenv "$ENV_PATH"
fi 

# activate virtualenv
source "$ENV_PATH/bin/activate"

pip install --upgrade pip
pip install -r "$PROJECT_ROOT/requirements.txt"
