from crawler import O2Crawler
from o2exceptions import O2CrawlerError
import logging

def main():
    """ Print the price of calling a landline in the listed countries
        from an O2 Pay Monthly contract
    """
    log = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)
    countries = (
        'Canada', 'Germany', 'Iceland',
        'Pakistan', 'Singapore', 'South Africa')

    with O2Crawler() as o2c:
        print "O2 landline rates when calling abroad from the UK:"
        for country in countries:
            try:
                rates = o2c.get_monthly_rates(country)
                print '%20s %10s' % (country, rates['Landline'])
            except O2CrawlerError:
                log.error(traceback.format_exc(country))


if __name__ == '__main__':
    main()