
class O2CrawlerError(Exception):
    pass


class InvalidCountryNameError(O2CrawlerError):
    pass


class NoRatesError(O2CrawlerError):
    pass