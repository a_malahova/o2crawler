# -*- coding: utf-8 -*-
from o2exceptions import O2CrawlerError, InvalidCountryNameError, NoRatesError
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import os

# Tell the Python bindings to use Marionette
# to allow Selenium WebDriver working with latest FF.
ff_caps = DesiredCapabilities.FIREFOX
ff_caps['marionette'] = True
# Point to the binary executable on Mac OS
ff_caps["binary"] = os.environ['FF_BINARY']

class O2Crawler(object):
    """
    A web crawler class for O2 international calling rates
    """
    # O2 web page for international calling prices
    url = 'http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk'

    def __init__(self):
        self.driver = webdriver.Firefox(capabilities=ff_caps)
        self.open = True

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self.driver.quit()
        self.open = False

    def input_text(self, id, text):
        """ Find the input element by id and fill in the passed text
        :param id: input element id
        :param text: text to fill in the input field
        """
        txt_input = self.driver.find_element_by_id(id)
        txt_input.clear()
        txt_input.send_keys(text)
        txt_input.send_keys(Keys.RETURN);
        if u'error' in txt_input.get_attribute("class"):
            raise InvalidCountryNameError(text)

    def click_element(self, id):
        """ Click the element with passed id
        """
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, id))
        )
        element.click()

    def get_monthly_rates(self, country):
        """ Get tarifs when calling from the UK abroad using O2 operator
        :param country: name of the country for outgoing communications
        :returns: a dictionary of [service: price], where service 
            is Landline, Mobile or Cost per text message.
        """
        std_rates = dict()
        # fetch the O2 web page
        self.driver.get(self.url)
        if "O2 | International | International Caller Bolt On"\
                not in self.driver.title:
            raise O2CrawlerError("Unable to open {}".format(self.url))
        # input country name in the search field
        self.input_text("countryName", country)
        # select tariff plan by clicking 'Pay Monthly' link
        self.click_element("paymonthly")
        # find all rows in the table with standard rates
        rates_tr = self.driver.find_elements_by_xpath(
            '//div[@id="paymonthlyTariffPlan"]//div[@id="standardRates"]//tr')
        # extract data from the table to the dictionary std_rates
        for r in rates_tr:
            std_rates[r.find_element_by_xpath('.//td[1]').get_attribute("textContent")] = \
                r.find_element_by_xpath('.//td[2]').get_attribute("textContent")
        if 'Landline' not in std_rates:
            raise NoRatesError() 

        return std_rates