# -*- coding: utf-8 -*-
from unittest import TestCase
from py.crawler import O2Crawler, O2CrawlerError, InvalidCountryNameError, NoRatesError

class O2CrawlerTest(TestCase):

    def test_o2c_init_and_exit(self):
        with O2Crawler() as o2c:
            self.assertTrue(o2c.open)
        self.assertFalse(o2c.open)

    def test_input_text(self):
        with O2Crawler() as o2c:
            o2c.driver.get(o2c.url)
            input_id = 'countryName'
            o2c.input_text(input_id, 'Fiji')
            self.assertEqual('Fiji',
                o2c.driver.find_element_by_id(input_id).get_attribute('value'))

    def test_click_element(self):
        with O2Crawler() as o2c:
            o2c.driver.get(o2c.url)
            o2c.input_text('countryName', 'Fiji')
            o2c.click_element('payandgo')
            self.assertNotIn('ui-tabs-hide',
                o2c.driver.find_element_by_id('payandgoTariffPlan').get_attribute('class'))

    def test_invalid_country(self):
        with O2Crawler() as o2c:
            self.assertRaises(InvalidCountryNameError, o2c.get_monthly_rates, 'Yugoslavia')

    def test_single_country_rates(self):
        with O2Crawler() as o2c:
            rates = o2c.get_monthly_rates('Ships/ferries/Airplane networks')
            self.assertDictEqual({
                u'Landline': u'£8.10',
                u'Mobiles': u'£8.10',
                u'Cost per text message': u'20p'
                },
                rates)

    def test_optimor_task(self):
        answers = {
            'Canada': u'£1.00',
            'Germany': u'£1.00',
            'Iceland': u'£1.00',
            'Pakistan': u'£1.20',
            'Singapore': u'£1.00',
            'South Africa': u'£1.00'}
        with O2Crawler() as o2c:
            for a in answers:
                rate = o2c.get_monthly_rates(a)
                self.assertEqual(rate['Landline'], answers[a])
